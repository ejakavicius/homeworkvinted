# READ-ME #


Shipping home work  
Version 1.0.1   
Language: scala  

## Program Description ##

This program outputs transactions and appends reduced shipment price and a shipment discount (or '-' if there is none). Program appends 'Ignored' word if the line format is wrong or carrier/sizes are unrecognised.

## Discount Rules ##


1.    All S shipments should always match lowest S package price among the providers.
2.    Third L shipment via LP should be free, but only once a calendar month.
3.    Accumulated discounts cannot exceed 10 € in a calendar month. If there is no enough funds to fully cover a discount this calendar month, it should be covered partially.


## Program Output ##
Program input is provided below.

Date  | Package Size | Provider | Price | Discount
----- | ------------ | -------- | ----- | ------ |
2015-02-01|S|MR|1.5|0.5
2015-02-02|S|MR|1.5|0.5
2015-02-03|L|LP|6.9|-
2015-02-05|S|LP|1.5|-
2015-02-06|S|MR|1.5|0.5
2015-02-06|L|LP|6.9|-
2015-02-07|L|MR|4|-
2015-02-08|M|MR|3|-
2015-02-09|L|LP|0.0|6.9
2015-02-10|L|LP|6.9|-
2015-02-10|S|MR|1.5|0.5
2015-02-10|S|MR|1.5|0.5
2015-02-11|L|LP|6.9|-
2015-02-12|M|MR|3|-
2015-02-13|M|LP|4.9|-
2015-02-15|S|MR|1.5|0.5
2015-02-17|L|LP|6.9|-
2015-02-17|S|MR|1.9|0.1
2015-02-24|L|LP|6.9|-
2015-02-28|CUSPS|Ignored
2015-03-01|S|MR|1.5|0.5

## Program Input ##

Date  | Package Size | Provider
----- | ------------ | --------
2015-02-01|S|MR
2015-02-02|S|MR
2015-02-03|L|LP
2015-02-05|S|LP
2015-02-06|S|MR
2015-02-06|L|LP
2015-02-07|L|MR
2015-02-08|M|MR
2015-02-09|L|LP
2015-02-10|L|LP
2015-02-10|S|MR
2015-02-10|S|MR
2015-02-11|L|LP
2015-02-12|M|MR
2015-02-13|M|LP
2015-02-15|S|MR
2015-02-17|L|LP
2015-02-17|S|MR
2015-02-24|L|LP
2015-02-29|CUSPS
2015-03-01|S|MR