import java.time.LocalDate

/**
  * Created by edvinas on 16.5.21.
  */
object Discounts{

  var counter: Int = 0
  var discountAlreadyGiven = false
  var date = LocalDate.now
  var monthlyBudget: BigDecimal = 0

  private def checkIfShippingFree(transaction: Transaction, size: Size, provider: Provider,
                                  row: Int, monthlyLimit: BigDecimal): BigDecimal = {
    val discount = Parsing.transactionPrice(transaction).get
    if (transaction.packageSize == size && transaction.provider == provider) {
      counter += 1
    }
    if (transaction.packageSize == size
      && transaction.provider == provider
      && counter == row
      && !discountAlreadyGiven)
    {
      discountAlreadyGiven = true
      counter = 0
      calculateBalance(discount, monthlyLimit)
    }else{
      0
    }
  }

  private def discountForParticularTrans(transaction: Transaction, monthlyLimit: BigDecimal): BigDecimal = {
    val discount = 0.5
    if(transaction.packageSize == S
      && transaction.provider == MR)
    {
      calculateBalance(discount, monthlyLimit)
    }else{0}
  }

  private def calculateBalance(discount: BigDecimal, monthlyLimit: BigDecimal): BigDecimal={
    if (monthlyBudget <= monthlyLimit - discount) {
      val tmp = monthlyBudget + discount
      monthlyBudget = tmp
      discount
    }
    else if (monthlyBudget < 10 && monthlyBudget > monthlyBudget - discount){
      val balance = monthlyLimit - monthlyBudget
      val tmp = monthlyBudget + balance
      monthlyBudget = tmp
      balance
    }else if(monthlyBudget >= monthlyLimit){
      0
    }else{0}
  }

  def getDiscount(transaction: Transaction, discountSize: Size, discountProvider: Provider, row: Int, monthlyLimit: BigDecimal): BigDecimal = {
    if (transaction.date.getMonth != date.getMonth) {
      date = transaction.date
      monthlyBudget = 0
      discountAlreadyGiven = false
      counter = 0
    }
    val freeShippingDiscount = checkIfShippingFree(transaction, discountSize, discountProvider, row, monthlyLimit)
    val transactionDiscount = discountForParticularTrans(transaction, monthlyLimit)
    freeShippingDiscount + transactionDiscount
  }

}
