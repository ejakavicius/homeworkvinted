import java.time.LocalDate
import java.time.format.{DateTimeParseException, DateTimeFormatter}

import scala.io.Source

object Parsing {

  private def dateStr2LocalDate(rawDate: String): Option[LocalDate] = {
    val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    try {
      Option(LocalDate.parse(rawDate, formatter))
    }
    catch {
      case e: DateTimeParseException => None
    }
  }

  private def sizeStr2Size(rawSize: String): Option[Size] = {
    rawSize match {
      case "S" => Option(S)
      case "M" => Option(M)
      case "L" => Option(L)
      case "CUSPS" => Option(CUSPS)
      case _ => None
    }
  }

  private def providerStr2Provider(rawProvider: String): Option[Provider] = {
    rawProvider match {
      case "LP" => Option(LP)
      case "MR" => Option(MR)
      case _ => None
    }
  }

  def rawLine2Transaction(line: String): Option[Transaction] = {
    val lineParts = line.split(" ")
    try{
      val date = dateStr2LocalDate(lineParts(0)).get
      val size = sizeStr2Size(lineParts(1)).get
      val provider = providerStr2Provider(lineParts(2)).get
      Option(Transaction(date, size, provider))
    }
    catch  {
      case a if dateStr2LocalDate(lineParts(0)).isDefined && sizeStr2Size(lineParts(1)).get == CUSPS =>
        Option(Transaction(dateStr2LocalDate(lineParts(0)).get, CUSPS, UnknownProvider))
      case e: Exception => None
    }
  }

  def parseFile(fileName: String): Seq[Transaction] = {
    val file = Source.fromFile(fileName)
    file.getLines.map(line => rawLine2Transaction(line)).toVector.filter(_.isDefined).map(_.get)
  }

  def transactionPrice(transaction: Transaction): Option[BigDecimal] =  {
    transaction match {
      case Transaction(date, S, LP) => Option(1.5)
      case Transaction(date, M, LP) => Option(4.9)
      case Transaction(date, L, LP) => Option(6.9)
      case Transaction(date, S, MR) => Option(2)
      case Transaction(date, M, MR) => Option(3)
      case Transaction(date, L, MR) => Option(4)
      case Transaction(date, CUSPS, _) => Option(0)
      case _ => None
    }
  }

}
