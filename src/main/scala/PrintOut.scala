
object PrintOut {

  private def printDiscount(discount: BigDecimal): String = {
    discount.toString match {
      case "0" => "-"
      case _ => discount.toString
    }
  }

  private def formatLine(transaction: Transaction, price: BigDecimal, discount: BigDecimal): Unit ={
    transaction match {
      case Transaction(date, CUSPS, _) => println(transaction.date + " " + transaction.packageSize.toString + " Ignored")
      case Transaction(_, _, _) => println(transaction.date + " " + transaction.packageSize.toString + " " +
        transaction.provider + " " + price + " " + printDiscount(discount))
      case _ => println("Something went wrong.")
    }
  }

  def transactionsWithDiscounts(transactions: Seq[Transaction], discountSize: Size, discountProvider: Provider, discountRow: Int, monthlyLimit: BigDecimal): Unit = {
    val pricesWithDiscount = transactions.map(t => Parsing.transactionPrice(t).get - Discounts.getDiscount(t, discountSize, discountProvider, discountRow, monthlyLimit))
    val transactionsWithPrices = transactions zip pricesWithDiscount
    val discounts = transactions.map(t => Discounts.getDiscount(t, discountSize, discountProvider, discountRow, monthlyLimit))
    val transactionsWithDiscounts = transactionsWithPrices zip discounts

    transactionsWithDiscounts.foreach(t => formatLine(t._1._1, t._1._2, t._2))
  }
}
