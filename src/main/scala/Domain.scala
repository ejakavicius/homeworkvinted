import java.time.LocalDate

sealed trait Size
case object S extends Size
case object M extends Size
case object L extends Size
case object CUSPS extends Size

sealed trait Provider
case object LP extends Provider
case object MR extends Provider
case object UnknownProvider extends Provider

case class Transaction(date: LocalDate, packageSize: Size, provider: Provider)

