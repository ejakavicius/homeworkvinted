object Shipping {

  def main(args: Array[String]): Unit = {
    val fileName = "input.txt"

//    Free shipping(once a month) item parameters
    val freeShippingProvider = LP
    val freeShippingSize = L
    val rowForFreeShipping = 3

//    Monthly limit of discounts
    val monthlyDiscountLimit = 10

    val transactions =  Parsing.parseFile(fileName)
    PrintOut.transactionsWithDiscounts(transactions, freeShippingSize,freeShippingProvider, rowForFreeShipping, monthlyDiscountLimit)
  }

}
