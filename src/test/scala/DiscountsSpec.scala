import java.time.LocalDate
import java.time.format.DateTimeFormatter

import org.scalatest.FlatSpec

class DiscountsSpec extends FlatSpec {
  "When transaction has partial discount and monthly budget is ok" should "return a discount of 0.5" in {
    val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val date = LocalDate.parse("2015-02-13", formatter)
    val transaction = Transaction(date, S, MR)
    val discount = Discounts.getDiscount(transaction, L, LP, 3, 10)
    assert(discount === 0.5)
  }

  "When transaction has no partial discount" should "return 0" in {
    val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val date = LocalDate.parse("2015-02-13", formatter)
    val transaction = Transaction(date, L, LP)
    val discount = Discounts.getDiscount(transaction, L, LP, 3, 10)
    assert(discount === 0)
  }


  "When transaction has partial discount and monthly budget is exceeded at the same month" should "return 0" in {
    val date = LocalDate.now
    Discounts.date = date
    Discounts.monthlyBudget = 10
    val transaction = Transaction(date, S, MR)
    val discount = Discounts.getDiscount(transaction, L, LP, 3, 10)
    assert(discount === 0)
  }

  "When transaction has partial discount and monthly budget is almost exceeded" should "return partial discount (0.3)" in {
    Discounts.monthlyBudget = 9.7
    val date = LocalDate.now
    val transaction = Transaction(date, S, MR)
    val discount = Discounts.getDiscount(transaction, L, LP, 3, 10)
    assert(discount === 0.3)
  }

  "When transaction is 1st in a row the same monthly budget is ok" should "return 0" in {
    val date = LocalDate.now
    val transaction = Transaction(date, L, LP)
    val discount = Discounts.getDiscount(transaction, L, LP, 3, 10)
    assert(discount === 0)
  }

  "When transaction is 3rd in a row in the same month, the monthly budget is ok" should "return full discount" in {
    val date = LocalDate.now
    Discounts.counter = 2
    Discounts.monthlyBudget = 0
    val transaction = Transaction(date, L, LP)
    val discount = Discounts.getDiscount(transaction, L, LP, 3, 10)
    assert(discount === 6.9)
  }

  "When transaction is 3rd in a row in the same month, the monthly budget is NOT ok" should "return partial discount" in {
    val date = LocalDate.now
    Discounts.counter = 2
    Discounts.monthlyBudget = 5.7
    Discounts.discountAlreadyGiven = false
    Discounts.date = date
    val transaction = Transaction(date, L, LP)
    val discount = Discounts.getDiscount(transaction, L, LP, 3, 10)
    assert(discount === 4.3)
  }

  "When transaction is 4rd in a row in the same month, the monthly budget is ok" should "return 0" in {
    val date = LocalDate.now
    Discounts.date = date
    Discounts.counter = 3
    Discounts.monthlyBudget = 6.9
    val transaction = Transaction(date, L, LP)
    val discount = Discounts.getDiscount(transaction, L, LP, 3, 10)
    assert(discount === 0)
  }

}
