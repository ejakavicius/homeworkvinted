import java.time.format.DateTimeFormatter
import java.time.LocalDate
import collection.mutable.Stack
import org.scalatest.FlatSpec

class ParsingSpec extends FlatSpec {
  "Empty line when parsed" should "return None" in {
    val line = ""
    val parsedLine = Parsing.rawLine2Transaction(line)
    assert(parsedLine === None)
  }

  "random noise like line when parsed" should "return None" in {
    val line = "kljfq;kldjflqhdf;lkjq;lkehflk;jwf;lq"
    val parsedLine = Parsing.rawLine2Transaction(line)
    assert(parsedLine === None)
  }

  "Valid line when parsed" should "return Some" in {
    val parsedLine1 = Parsing.rawLine2Transaction("2015-02-13 S MR")
    val parseSuccess1 = parsedLine1 match {
      case None => false
      case Some(_) => true
    }
    assert(parseSuccess1)
  }

  "Valid transaction line when parsed" should "return Transaction" in {
    val parsedLine1 = Parsing.rawLine2Transaction("2015-02-13 S MR")
    val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val date = LocalDate.parse("2015-02-13", formatter)
    assert(parsedLine1.get.provider === MR)
    assert(parsedLine1.get.packageSize === S)
    assert(parsedLine1.get.date === date)
  }
}
